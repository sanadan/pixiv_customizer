// ==UserScript==
// @name         Pixiv customizer
// @namespace    https://javelin.works
// @version      0.4.3
// @description  Pixivをカスタマイズ
// @author       sanadan
// @include      https://www.pixiv.net/users/*
// @grant        none
// ==/UserScript==

(function () {
  'use strict'

  // ページネーションの複製処理
  function copyPagenation () {
    // ページネーションの代わりの画面変更ボタンを探す
    let elements = document.getElementsByClassName('sc-LzLwn')
    elements.forEach(e => {
      e.addEventListener('click', function () {
        setTimeout(copyPagenation, 1000)
      })
    })
    if (elements.length > 0) {
      return -1
    }

    // ページネーションボタンを探す
    const pagenations = document.getElementsByClassName('_1zRQ9vu')
    const insertPositions = document.getElementsByClassName('sc-9y4be5-0')
    if (insertPositions.length === 1) {
      if (pagenations.length === 1) {
        const element = pagenations[0]
        element.style = 'margin-bottom: 20px;'
        insertPositions[0].insertAdjacentHTML('beforebegin', element.outerHTML)
      } else if (pagenations.length === 2) {
        pagenations[0].innerHTML = pagenations[1].innerHTML
      }

      if (pagenations.length > 0) {
        // ページネーションボタンイベントに、ページネーション更新処理を追加する
        const elements = document.getElementsByClassName('_2m8qrc7')
        elements.forEach(e => {
          e.addEventListener('click', function () {
            setTimeout(copyPagenation, 1000)
          })
        })
      }
    }

    // タブボタンを探す
    elements = document.getElementsByClassName('sc-LzLtf')
    elements.forEach(e => {
      e.addEventListener('click', function () {
        setTimeout(copyPagenation, 1000)
      })
    })

    return pagenations.length
  }

  var root = document.getElementById('root')
  if (root) {
    root.insertAdjacentHTML('beforeend', `
<ul class="_1gG2Yro" style="top:32px;bottom:unset;">
  <li class="_1rooQfI">
    <button id="go_to_bottom" class="_3cJzhTE" style="background-color: rgb(0, 0, 0);transform: rotateX(180deg);">
      <svg viewBox="0 0 120 120" class="_3Fo0Hjg">
        <polyline points="60,105 60,8"></polyline>
        <polyline points="10,57 60,8 110,57"></polyline>
      </svg>
    </button>
  </li>
</ul>`
    )

    // ページネーション複製のタイミング監視
    let retry = 10
    const id = setInterval(function () {
      retry -= 1
      const length = copyPagenation()
      if (length !== 0 || retry <= 0) {
        clearInterval(id)
      }
    }, 1000)
  } else {
    document.body.insertAdjacentHTML('beforeend', `
<ul class="_toolmenu" style="top:32px;bottom:unset;">
  <li id="go_to_bottom" class="item">
    <i class="_icon-12 _icon-up" style="transform: rotateX(180deg);"></i>
  </li>
</ul>`
    )
  }

  document.getElementById('go_to_bottom').addEventListener('click', function () {
    var figures = document.getElementsByTagName('figure')
    var element = null
    if (figures.length > 0) {
      element = figures[0]
    } else {
      element = document.body
    }
    window.scrollTo(0, element.getBoundingClientRect().bottom + window.pageYOffset)
  }, false)
})()
